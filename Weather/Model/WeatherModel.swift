//
//  WeatherModel.swift
//  Weather
//
//  Created by Rameez Khan on 6/14/23.
//

import Foundation

struct WeatherModel {
    let cityName: String
    let temperature: Double
    let status: String
    let feelsLike: Double
    let icon: String
    let description: String
    
    var tempString: String {
        if temperature == 0.0 {
            return ""
        }
        return "\(String(lround(temperature)))°"
    }
    var feelsLikeString: String {
        if feelsLike == 0.0 {
            return ""
        }
        return "Feels like \(String(lround(feelsLike)))°"
    }
    var imageUrl: URL? {
        return URL(string: "https://openweathermap.org/img/wn/\(icon)@2x.png")
    }
    
    static let noWeather = WeatherModel(cityName: "", temperature: 0.0, status: "", feelsLike: 0.0, icon: "", description: "")
    static let defaultWeather = WeatherModel(cityName: "Chicago", temperature: 61.0, status: "Cloudy", feelsLike: 60.0, icon: "04n", description: "Scattered clouds")
}

struct WeatherModelCollection {
    var sample: WeatherModel
}
