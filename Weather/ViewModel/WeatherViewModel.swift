//
//  WeatherViewModel.swift
//  Weather
//
//  Created by Rameez Khan on 6/14/23.
//

import Foundation

class WeatherViewModel: ObservableObject {
    @Published var weather: WeatherModel = WeatherModel.noWeather
//    @Published var error: Error =
    
    let baseUrlString = "https://api.openweathermap.org/data/2.5/weather?appid=1ae0713c5b8232fa451dad2ae24ae705&units=imperial"

    enum FetchError: Error {
        case badRequest
        case badJSON
        case invalidURL
        case dataMissing
    }
    
    /// fetch weather data using city name
    func fetchWeather(using cityName: String) {
        let queryString = cityName.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let urlString = "\(baseUrlString)&q=\(queryString)"
        fetchData(with: urlString)
    }
    
    /// fetch weather data using latitude and longitude
    func fetchWeather(latitude: String, longitude: String) {
        let urlString = "\(baseUrlString)&lat=\(latitude)&lon=\(longitude)"
        fetchData(with: urlString)
    }
    
    private func fetchData(with urlString: String) {
        guard let url = URL(string: urlString) else {
            print("Error is ", FetchError.invalidURL)
            return
        }
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if error != nil {
                print("Error is ", error.debugDescription)
                return
            }
            guard let data = data else {
                //handle data error
                print("Error is ", FetchError.dataMissing)
                return
            }
            if let weather = self.parseJSON(data) {
                DispatchQueue.main.async {
                    self.weather = weather
                }
            }
        }
        task.resume()
    }
    
    private func parseJSON(_ weatherData: Data) -> WeatherModel? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(WeatherData.self, from: weatherData)
            let temp = decodedData.main.temp
            let name = decodedData.name
            let status = decodedData.weather[0].main
            let feelsLike = decodedData.main.feelsLike
            let icon = decodedData.weather[0].icon
            let description = decodedData.weather[0].description
            
            let weather = WeatherModel(cityName: name, temperature: temp, status: status, feelsLike: feelsLike, icon: icon, description: description)
            return weather
            
        } catch {
            print("Error is ", FetchError.badJSON)
            return nil
        }
    }
}
