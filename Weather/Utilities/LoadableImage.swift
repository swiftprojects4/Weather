//
//  LoadableImage.swift
//  Weather
//
//  Created by Rameez Khan on 6/14/23.
//

import SwiftUI

struct LoadableImage: View {
    var weatherdata: WeatherModel
    
    var body: some View {
        CachedAsyncImage(url: weatherdata.imageUrl) { phase in
            if let image = phase.image {
                image
                    .resizable()
                    .scaledToFit()
                    .shadow(radius: 5)
                    .accessibility(hidden: false)
                    .accessibilityLabel(Text(weatherdata.description))
            }  else if phase.error != nil  {
                Image(systemName: "exclamationmark.triangle.fill")
                    .resizable()
                    .scaledToFit()
            } else {
                ProgressView()
            }
        }
    }
}
