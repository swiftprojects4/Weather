//
//  WeatherApp.swift
//  Weather
//
//  Created by Rameez Khan on 6/13/23.
//

import SwiftUI

@main
struct WeatherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
