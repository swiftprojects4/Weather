//
//  ContentView.swift
//  Weather
//
//  Created by Rameez Khan on 6/13/23.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject var weatherModel = WeatherViewModel()
    @StateObject var locationManager = LocationManager()
        
    @State private var searchText = ""
    @State private var locationDisabledText = ""
    
    @AppStorage("lastSearchedCity") private var lastCity = ""
    
    let gradient = LinearGradient(colors: [Color.teal,Color.blue],
                                  startPoint: .top, endPoint: .bottom)
    
    var body: some View {
        NavigationStack {
            ZStack {
                gradient
                   .opacity(0.5)
                   .ignoresSafeArea()
                VStack(alignment: .center, spacing: 20) {
                    HStack {
                        VStack {
                            Text(weatherModel.weather.cityName)
                                .font(.largeTitle)
                                .fontWeight(.bold)
                                .multilineTextAlignment(.center)
                            Text(weatherModel.weather.tempString)
                                .font(.system(size: 100))
                                .fontWeight(.light)
                            
                        } //: VSTACK
                        VStack {
                            LoadableImage(weatherdata: weatherModel.weather)
                                .frame(width: 150, height: 150)
                                .padding(.vertical, -45)
                            Text(weatherModel.weather.status)
                                .font(.title)
                                .minimumScaleFactor(0.7)
                                .lineLimit(1)
                            Text(weatherModel.weather.feelsLikeString)
                                .font(.title3)
                                .fontWeight(.medium)
                                .multilineTextAlignment(.center)
                                .padding(.vertical, 10)
                            Text(locationDisabledText)
                                .multilineTextAlignment(.center)
                        } //: VSTACK
                    } //: HSTACK
                    Spacer()
                } //: VSTACK
                .padding(.horizontal)
            .padding(.vertical, 50)
            }//: ZSTACK
            .onReceive(locationManager.$location, perform: { location in
                let latitude = "\(location?.coordinate.latitude ?? 0)"
                let longitude = "\(location?.coordinate.longitude ?? 0)"
                if (locationManager.statusString == "authorizedWhenInUse" || locationManager.statusString == "authorizedAlways") && latitude != "0.0" && longitude != "0.0" {
                        weatherModel.fetchWeather(latitude: latitude, longitude: longitude)
                    locationDisabledText = ""
                } else if !lastCity.isEmpty {
                    weatherModel.fetchWeather(using: lastCity)
                    locationDisabledText = ""
                } else {
                    locationDisabledText = "Please enable location or\n search using the city name!"
                }
            })
        }
        .searchable(text: $searchText, prompt: "Search a city")
        .onSubmit(of: .search) {
            lastCity = searchText
            weatherModel.fetchWeather(using: searchText)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
